import React from "react";

class LocationForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {  // setup initial states as empty
      name: '',
      roomCount: '',
      city: '',
      states: []
     };

// bind functions to the class

    this.handleNameChange = this.handleNameChange.bind(this); 
    this.handleCityChange = this.handleCityChange.bind(this)
    this.handleStateChange = this.handleStateChange.bind(this);
    this.handleRoomCountChange = this.handleRoomCountChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this);

// handle changes to the form. Event is parameter
  }
  handleNameChange(event) {
    const value = event.target.value;
    this.setState({name: value})
  }

  handleCityChange(event) {
    const value = event.target.value;
    this.setState({city: value})
  }

  handleRoomCountChange(event) {
    const value = event.target.value;
    this.setState({roomCount: value})
  }
  handleStateChange(event) {
    const value = event.target.value;
    this.setState({state: value})
  }

// handle submission. Reshape the data, based on how the API takes data

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state}; // make copy of state, assign it to 'data'
    data.room_count = data.roomCount; // create new key room_count to match API
    delete data.roomCount; // delete old key 'roomCount' as we don't need it 
    delete data.states; // delete 'states'
    console.log(data);
  
  const locationUrl = 'http://localhost:8000/api/locations/';
  const fetchConfig = {
    method: "post",
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const response = await fetch(locationUrl, fetchConfig);
  if (response.ok) {
    const newLocation = await response.json();
    console.log(newLocation);
    
    // clear form
    const cleared = {
      name: '',
      roomCount: '',
      city: '',
      state: '',
    };
    this.setState(cleared);
  }
}

  // GET data, once component is mounted

  async componentDidMount() {
    const url = "http://localhost:8000/api/states/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ states: data.states });
      // console.log("DATA:", data.states);
    }
  }

  // render the page
  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit={this.handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input
                  value={this.state.name}
                  onChange={this.handleNameChange}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={this.state.roomCount}
                  onChange={this.handleRoomCountChange}
                  placeholder="Room count"
                  required
                  type="number"
                  name="room_count"
                  id="room_count"
                  className="form-control"
                />
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={this.state.city}
                  onChange={this.handleCityChange}
                  placeholder="City"
                  required
                  type="text"
                  name="city"
                  id="city"
                  className="form-control"
                />
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
                <select
                  value={this.state.state}
                  onChange={this.handleStateChange}
                  required
                  name="state"
                  id="state"
                  className="form-select"
                >
                  <option value="">Choose a state</option>
                  {this.state.states.map((state) => {
                    return (
                      <option key={state.abbreviation} value={state.abbreviation}>{state.name}</option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default LocationForm;
