// old approach


import React from "react";

class PresentationForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      presenterName: '',
      companyName: '',
      presenterEmail: '',
      title: '',
      synopsis: '',
      created: '',
      conferences: [],
    };
  }

  async componentDidMount() {
    const url = "http://localhost:8000/api/conferences/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ conferences: data.conferences });
      console.log(data.conferences)
    }
    this.handleNameChange = this.handleNameChange.bind(this);


  }

  // handle submission 
  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    data.presenter_name = data.presenterName;
    data.company_name = data.companyName;
    data.presenter_email = data.presenterEmail;
    delete data.presenterName;
    delete data.companyName;
    delete data.presenterEmail;
    delete data.conferences;

    const attendeeUrl = 'http://localhost:8000/api/presentations/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const presentationResponse = await fetch(attendeeUrl, fetchOptions);
    if (presentationResponse.ok) {
      this.setState({
        presenterName: '',
        companyName: '',
        presenterEmail: '',
        title: '',
        synopsis: '',
        created: '',
        conference: '',
      });
    }
  }

// states: presenter_name, company_name, presenter_email, title, synopsis, created

handlePresenterNameChange(event) {
  const value = event.target.value;
  this.setState({presenterName: value})
}
handleComanyNameChange(event) {
  const value = event.target.value;
  this.setState({companyName: value})
}
handlepresenterEmailChange(event) {
  const value = event.target.value;
  this.setState({presenterEmail: value})
}
handleTitleChange(event) {
  const value = event.target.value;
  this.setState({title: value})
}
handleSynopsisChange(event) {
  const value = event.target.value;
  this.setState({synopsis: value})
}
handleCreatedChange(event) {
  const value = event.target.value;
  this.setState({created: value})
}
handleConferenceChange(event) {
  const value = event.target.value;
  this.setState({created: value})
}


  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Presentation</h1>
            <form onSubmit={this.handleSubmit}
            id="create-presentation-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handlePresenterNameChange}
                  value = {this.state.presenterName}
                  placeholder="presenter name"
                  required
                  type="text"
                  id="presenter_name"
                  name="presenter_name"
                  className="form-control"
                />
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                                  onChange={this.handlepresenterEmailChange}
                                  value = {this.state.presenterEmail}
                  placeholder="presenter_email count"
                  required
                  type="email"
                  id="presenter_email"
                  name="presenter_email"
                  className="form-control"
                />
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input
                                  onChange={this.handleComanyNameChange}
                                  value = {this.state.companyName}
                  placeholder="company_name"
                  required
                  type="text"
                  id="company_name"
                  name="company_name"
                  className="form-control"
                />
                <label htmlFor="company_name">Company Name (optional)</label>
              </div>
              <div className="form-floating mb-3">
                <input
                                  onChange={this.handleTitleChange}
                                  value = {this.state.title}
                  placeholder="title"
                  required
                  type="text"
                  id="title"
                  name="title"
                  className="form-control"
                />
                <label htmlFor="title">Title of Presentation</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea
                                  onChange={this.handleSynopsisChange}
                                  value = {this.state.synopsis}
                  required
                  type="text"
                  id="synopsis"
                  name="synopsis"
                  className="form-control"
                  rows="3"
                ></textarea>
              </div>
              <div className="mb-3">
                <select
                                  onChange={this.handleConferenceChange}
                                  value = {this.state.conference}
                  required
                  id="conference"
                  name="conference"
                  className="form-select"
                >
                  <option value="">Select a Conference</option>
                  {this.state.conferences.map((conference) => {
                    return (
                      <option key={conference.href} value={conference.href}>
                        {conference.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default PresentationForm;
