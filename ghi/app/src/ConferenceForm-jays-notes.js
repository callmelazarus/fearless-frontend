import React from "react";

class ConferenceForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
        name: '',
        starts: '',
        ends: '',
        description: '',
        maxAttendees: '',
        maxPresentations: '',
        locations: [] 
    };

    // bind functions to the class

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleStartsChange = this.handleStartsChange.bind(this);
    this.handleEndsChange = this.handleEndsChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handlePresentationChange = this.handlePresentationChange.bind(this);
    this.handleAttendeesChange = this.handleAttendeesChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

  }

  // handle changes to the form. name, starts, ends, description, max_presentations, max_attendees, location

  handleNameChange(event) {
    const value = event.target.value;
    this.setState({ name: value });
  }
  handleStartsChange(event) {
    const value = event.target.value;
    this.setState({ starts: value });
  }
  handleEndsChange(event) {
    const value = event.target.value;
    this.setState({ ends: value });
  }
  handleDescriptionChange(event) {
    const value = event.target.value;
    this.setState({ description: value });
  }
  handlePresentationChange(event) {
    const value = event.target.value;
    this.setState({ maxPresentations: value });
  }
  handleAttendeesChange(event) {
    const value = event.target.value;
    this.setState({ maxAttendees: value });
  }
  handleLocationChange(event) {
    const value = event.target.value;
    this.setState({ location: value });
  }

  // handle submission. Reshape the data, based on how the API takes data

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state}; // make copy of state, assign it to 'data'
    data.max_presentations = data.maxPresentations; // create new key max_presentations to match API
    data.max_attendees = data.maxAttendees; // create new key max_attendees to match API
    delete data.maxPresentations; // delete old key 'maxPresentations' as we don't need it 
    delete data.maxAttendees; // delete old key 'maxAttendees' as we don't need it 
    delete data.locations; // delete 'locations'
    console.log(data);
  
  const conferenceUrl = 'http://localhost:8000/api/conferences/';
  const fetchConfig = {
    method: "post",
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const response = await fetch(conferenceUrl, fetchConfig);
  if (response.ok) {
    const newConference = await response.json();
    console.log(newConference);
    
    // clear form
    const cleared = {
        name: '',
        starts: '',
        ends: '',
        description: '',
        maxAttendees: '',
        maxPresentations: '',
        location: 'Choose a location'
    };
    this.setState(cleared);
  }
}

  // GET data, once component is mounted

  async componentDidMount() {
    const url = "http://localhost:8000/api/locations/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ locations: data.locations });
      //   console.log("DATA:", data.locations);
    }
  }

  // render the page

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleNameChange}
                  value={this.state.name}
                  placeholder="Name"
                  required
                  type="text"
                  id="name"
                  name="name"
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>

              <div className="form-floating mb-3">
                <input
                  onChange={this.handleStartsChange}
                  value={this.state.starts}
                  placeholder="Starts"
                  required
                  type="date"
                  id="starts"
                  name="starts"
                  className="form-control"
                />
                <label htmlFor="starts">Starts</label>
              </div>

              <div className="form-floating mb-3">
                <input
                  onChange={this.handleEndsChange}
                  value={this.state.ends}
                  placeholder="ends"
                  required
                  type="date"
                  id="ends"
                  name="ends"
                  className="form-control"
                />
                <label htmlFor="ends">Ends</label>
              </div>

              <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea
                  onChange={this.handleDescriptionChange}
                  value={this.state.description}
                  required
                  type="text"
                  id="description"
                  name="description"
                  className="form-control"
                  rows="3"
                ></textarea>
              </div>

              <div className="form-floating mb-3">
                <input
                  onChange={this.handlePresentationChange}
                  value={this.state.maxPresentations}
                  placeholder="max_presentations"
                  required
                  type="number"
                  id="max_presentations"
                  name="max_presentations"
                  className="form-control"
                />
                <label htmlFor="max_presentations">Max Presentations</label>
              </div>

              <div className="form-floating mb-3">
                <input
                  onChange={this.handleAttendeesChange}
                  value={this.state.maxAttendees}
                  placeholder="max_attendees"
                  required
                  type="number"
                  id="max_attendees"
                  name="max_attendees"
                  className="form-control"
                />
                <label htmlFor="max_attendees">Max Attendees</label>
              </div>

              <div className="mb-3">
                <select
                  onChange={this.handleLocationChange}
                  value={this.state.location}
                  required
                  id="location"
                  name="location"
                  className="form-select"
                >
                  <option value="">Choose a location</option>
                  {this.state.locations.map((location) => {
                    return (
                      <option key={location.id} value={location.id}> 
                        {location.name}
                      </option>
                    );
                  })}
                </select>
              </div>

              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ConferenceForm;
